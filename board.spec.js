var Board = require('../dist/lib/board').default;
var Cell = require('../dist/lib/cell').default;
var GameEntity = require('../dist/lib/game-entity').default;
var GameEntityType = require('../dist/lib/game-entity').GameEntityType;

describe('Board', function(){   
    it('Given no arguments 1x1 board is produced', function(){
        var board = new Board();

        expect(board.rows).toBe(1);
        expect(board.cols).toBe(1);

        expect(board.grid[0][0] instanceof Cell).toBeTruthy();
    });

    it('Given no arguments 1x1 board is produced and its value is an Empty GameEntity', function(){
        var board = new Board();

        expect(board.grid[0][0].entity instanceof GameEntity).toBeTruthy();
        expect(board.grid[0][0].entity.type).toBe(GameEntityType.Empty);
    });

    it('Given a single argument N a board sized Nx1 is broduced', function(){

        var N = Math.ceil(Math.random()*10);

        var board = new Board(N);

        expect(board.rows).toBe(N);
        expect(board.cols).toBe(1);
    });

    it('Given arguments N and M a board sized NxM is broduced', function(){
        var N = Math.ceil(Math.random()*10);
        var M = Math.ceil(Math.random()*10);

        var board = new Board(N, M);

        expect(board.rows).toBe(N);
        expect(board.cols).toBe(M);
    });


    
    it('Given arguments N and M a board sized NxM is broduced and every cell has an Empty GameEntity', function(){
        var N = Math.ceil(Math.random()*10);
        var M = Math.ceil(Math.random()*10);

        var board = new Board(N, M);

        expect(board.rows).toBe(N);
        expect(board.cols).toBe(M);


        board.grid.forEach(rows => {
            rows.forEach(cell=>{
                expect(cell.entity.type).toBe(GameEntityType.Empty);
            });
        });
    });
});