var Cell = require('../dist/lib/cell').default;

describe('Cell', function(){
    var cell = new Cell();

    it('Creation succeeds.', function(){
        expect(cell instanceof Cell).toBeTruthy();
    });

    it('Default values are correct.', function(){
        expect(cell.value).toEqual(undefined);
        expect(cell.status).toEqual(false);
    });
    
    it('Has status of true (on) or false (off)', function(){
        var cell = new Cell(5);
        expect(cell.status).toBeFalsy();

        var cell = new Cell(5, true);
        expect(cell.status).toBeTruthy();
        
        cell.status = true;
        expect(cell.status).toBeTruthy();
    });
});