var Entity = require('../dist/lib/entity').default;
var GameEntityType = require('../dist/lib/game-entity').GameEntityType;
var GenericError = require('../dist/lib/errors').GenericError;

describe('Entity', function(){
    it('Given a valid arguments an Entity is created', function(){
        var e = new Entity(GameEntityType.Player);
        expect(e instanceof Entity).toBeTruthy();
    });

    it('Given no arguments exception is thrown', function(){
        
        expect(function(){
            var e = new Entity();
        }).toThrowError(GenericError.TooFewArguments);
    });
});