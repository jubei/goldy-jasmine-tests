  var GameEntityType = require('../dist/lib/game-entity').GameEntityType;
  var GameEntity = require('../dist/lib/game-entity').default;

  describe('Game Entity', function(){
      it('When created at (X, Y) navigates correctly.', function(){
        var column = Math.floor(Math.random()*50); var row = Math.floor(Math.random()*50);
        var e = new GameEntity(GameEntityType.Empty, row, column);
    
        e.moveLeft();
        expect(e.col).toBe(column-1);
    
        e.moveUp();
        expect(e.row).toBe(row+1);
    
        e.moveRight();
        expect(e.col).toBe(column);
    
        e.moveDown();
        expect(e.row).toBe(row);
    });
  });
