var Game = require('./Components/game').Game;
var GamePlayer = require('./Components/GamePlayer/game-player').default;
var Level = require('./Components/game').Level;
var Player = require('./Components/Player/player').default;
var GameError = require('./Components/Errors/errors').GameError;

function display(board){
    console.log("\n");
    board.grid.forEach(row => {
        var accumulateRow = " ";
        row.forEach(cell => {
            let state = cell.entity.type;
            if (!cell.entity.onBoard) state = "Off";
            accumulateRow = accumulateRow + state + " ";
        });
        console.log(accumulateRow, "\n");
    });
}

describe('Game', function(){
    it('Given Level 1 Game has a Board OF DIMENTIONS 9X9', function(){
        let g = new Game(new Level(0));
        expect(g.board.rows).toBe(9);
        expect(g.board.cols).toBe(9);
    });

    it('Given a Level with a Player – a players is created inside Game object', function(){
        let g = new Game(new Level(0));

        expect(g.players[0].flatLander.row).toBe(7);
        expect(g.players[0].flatLander.col).toBe(2);
    });

    it('Given a Level without a Player – the latter has its coorinates set at random Empty position', function(){
        let g = new Game(new Level(1));
        let p = new GamePlayer('Richard', new Player());
        
        g.addPlayer(p);
 
        expect(g.players[0]).toBe(p);
        expect(g.players[0].row).not.toBe(NaN);
        expect(g.players[0].col).not.toBe(NaN);
    });


    it('Given unsolvable Level throws an error', function(){
        expect(function(){
            let g = new Game(new Level(2));        
        }).toThrowError(GameError.Unsolvable);
    });    
});

describe('Game simulator', function(){
    it('Given a level,'+
       'followed by a sequence of navigations by player[0]'+
       'his/hers score increases by one,'+
       'but only if some Goldie reaches its destination', function(){
        var g = new Game(new Level(0));
        var p = g.players[0];

        p.flatLander.Up();
        p.flatLander.Up();
        p.flatLander.Up();
        
        display(g.board);
        expect(p.score).toBe(1);
    });
});